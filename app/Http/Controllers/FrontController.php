<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\Product;

class FrontController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('index',compact('products'));
    }

    public function contact(Request $request){
        $this->validate($request,[
            'email'          => 'string',
            'password'       => 'string|nullable',
            'company'        => 'string|nullable',
            'siret'          => 'string|nullable',
            'message'        => 'string|nullable',
            'birth_place'    => 'string|nullable',
            'birthday'       => 'string|nullable',
            'phone'          => 'string|nullable',
        ]);
        $data = [
            'email' => $request->email,
            'phone' => $request->phone,
            'company' => $request->company,
            'birthday' => $request->birthday,
            'birth place' => $request->birth_place,
            'message' => $request->message

        ];
        Mail::to($data["email"])->send(new SendEmail($data));
        return back()->with('success', "Email sent, checkout your mail!");
    }
}
