
<div class="row first">
    <h2>Ride every wave as if it's your last</h2>
    <p>We love the motion of the ocean</p>
</div>
<div class="row second">
@include('partials.tabs.home.slider')
</div>
<div class="sub-container">
    <h2 class="transparent-title">About us</h2>
    <div class="row third">
        <div class="third-left">
            <div class="custom-about">
                <h3>We are surfers .co</h3>
                <p>x</p>
                <p>Lorem ipsum etc..</p>
            </div>
        </div>
        <div></div>
    </div>
    <div class="row fourth">
        <div class="rd-border">
            <p>Instagram & co</p>
        </div>
        <div class="content custom-form">
        @include('partials.tabs.home.form')
        </div>
    </div>
</div>