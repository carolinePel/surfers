<div id="carouselExampleControls" class="carousel slide" data-interval=false data-ride="carousel">
  <div class="carousel-inner full-height">
    @foreach($products as $product)
    <div class="carousel-item full-height {{ $loop->first ? 'active' : '' }}">      <div class="row full-height slider_phone_display">
            <div class="col_phone_slider">
                <img class="d-block w-100 custom_slider_image" src="{{asset('assets/images/products/surfers-co_07.png')}}" alt="">
            </div>
        </div>
        <div class="row full-height slider_desktop_display">
            <div class="col-5 full-height custom_Vcenter">    
                <img class="d-block w-100 custom_slider_image" src="{{asset('assets/images/products/surfers-co_07.png')}}" alt="">
            </div>
            <div class="col-6">
                <div class="carousel-caption d-none d-md-block">
                    <h3>{{$product->name}}</h3>
                    <p>{{$product->type}}</p>
                    <p>{{$product->description}}</p>
                    <p>{{$product->features}}</p>
                    <p>{{$product->dimensions}}</p>
                    <p>{{$product->likes}}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span id="custom_control_icon" class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
  </a>
</div>