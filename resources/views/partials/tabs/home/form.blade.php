@if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <ul>
    @foreach($errors->all() as $error)
      <li>{{$error}}</li>
    @endforeach
    </ul>
  </div>
@endif
@if($message = Session::get('success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">x</button>
      <strong>{{ $message }}</strong>
  </div>
@endif
<form action="{{route('contact')}}" method="post" enctype="multipart/form-data"/>
  {{ csrf_field() }}
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="nameHelp" placeholder="Name">
  </div>
  <div class="form-group">
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-row">
    <div class="form-group col-6">
      <input name="birth_place" type="text" class="form-control" id="exampleInputBirthPlace1" placeholder="Your birth place">
    </div>
    <div class="form-group col-6">
      <input name="birthday" type="date" class="form-control" id="exampleInputBirthday1" placeholder="Your birth day">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-6">
      <input name="phone" type="text" class="form-control" id="exampleInputPhone1" placeholder="Your phone">
    </div>
    <div class="form-group col-6">
      <input name="company" type="text" class="form-control" id="exampleInputCompany1" placeholder="Your Company">
    </div>
  </div>
  <div class="form-group">
    <input name="message" type="description" class="form-control" id="exampleInputMessage1" placeholder="Your message">
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Accept privacy policy</label>
  </div>
  <button type="submit" class="btn custom-btn">Send</button>
</form>