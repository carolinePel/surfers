<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.google.com/specimen/Raleway?selection.family=Raleway:400,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.google.com/specimen/Montserrat?query=montserrat&selection.family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.google.com/specimen/Playfair+Display?category=Serif&selection.family=Playfair+Display:400,400i" rel="stylesheet" type="text/css">
        <link href="https://fonts.google.com/specimen/Roboto+Condensed?selection.family=Roboto+Condensed:700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        
        {{-- Bootstrap css --}}
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
    </head>
    <body>
      <div class="custom_container">
        @include('partials.menu')
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            @yield('home')
          </div>
          <div class="tab-pane fade" id="accessories" role="tabpanel" aria-labelledby="accessories-tab">
            @yield('accessories')
          </div>
          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
          </div>
        </div>
      </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
