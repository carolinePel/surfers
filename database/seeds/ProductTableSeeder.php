<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $description = "Easy to ride, care free surfing craft that's fun, for everyone, paddles well, super fast dows the line and freat for any level of surfing depending on who rides it. The Donny Stoker, a rework on design from stokesy'z first ever pro model. A super Easy to surf and fun board. The donny Stoker is a board for Total freesurfing expression.";

        $list = [
            [
                'name' => "JR Surfboards The Donny Stoker",
                'type' => "Yellow/Green Rail Fade",
                'likes' => 52,
                'description' => $description,
                "price" => 499.99,
                'features' => "",
                "dimensions" => "33 x 120 x 10",
                'created_at' => new \DateTime(),
            ],
            [
                'name' => "Whatever SurfBoard",
                'type' => 'lazy one',
                'likes' => 22,
                "description" => "For those who don't like surfing",
                "price" => 120,
                "features" => "pink",
                "dimensions" => "10 x 10 x 10",
                'created_at' => new \DateTime(),
            ],
            [
                'name' => "Barbie SurfBoard",
                'type' => 'pink one',
                'likes' => 10,
                "description" => "For those who like pink",
                "price" => 1209.99,
                "features" => "pink too",
                "dimensions" => "20 x 10 x 10",
                'created_at' => new \DateTime(),
            ],
            [
                'name' => "My SurfBoard",
                'type' => 'nonexistant',
                'likes' => 42,
                "description" => "For those who prefer do something else",
                "price" => 00.99,
                "features" => "transparent",
                "dimensions" => "20 x 10 x 10",
                'created_at' => new \DateTime(),
            ],
        ];

        DB::table('products')->insert($list);

    }
}
