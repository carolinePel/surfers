<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name',100)->nullable();
            $table->string('type',100)->default('default');
            $table->unsignedSmallInteger('likes')->default(0);
            $table->longText('description')->nullable();
            $table->text('features')->nullable();
            $table->string('dimensions')->nullable();
            $table->unsignedDecimal('price', 6, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
